import axios from 'axios';
import {setAlert} from './alert';
import {ADD_POST, DELETE_POST, GET_POSTS, POST_ERROR, UPDATE_LIKES, GET_POST, ADD_COMMENT, REMOVE_COMMENT} from './types';

export const getPosts = () => async dispatch => {
    try {
        const res = await axios.get('https://arcane-tor-47520.herokuapp.com/api/posts');
        dispatch({type: GET_POSTS, payload: res.data});
    } catch (err) {
        dispatch({type: POST_ERROR, payload: {msg: err.response.statusText, status: err.response.status}});
    }
}

export const getPost = id => async dispatch => {
    try {
        const res = await axios.get(`https://arcane-tor-47520.herokuapp.com/api/posts/${id}`);
        dispatch({type: GET_POST, payload: res.data});
    } catch (err) {
        dispatch({type: POST_ERROR, payload: {msg: err.response.statusText, status: err.response.status}});
    }
}

export const addLike = postID => async dispatch => {
    try {
        const res = await axios.put(`https://arcane-tor-47520.herokuapp.com/api/posts/like/${postID}`);
        dispatch({type: UPDATE_LIKES, payload: {postID, likes: res.data}});
    } catch (err) {
        dispatch({type: POST_ERROR, payload: {msg: err.response.statusText, status: err.response.status}});
    }
}

export const removeLike = postID => async dispatch => {
    try {
        const res = await axios.put(`https://arcane-tor-47520.herokuapp.com/api/posts/unlike/${postID}`);
        dispatch({type: UPDATE_LIKES, payload: {postID, likes: res.data}});
    } catch (err) {
        dispatch({type: POST_ERROR, payload: {msg: err.response.statusText, status: err.response.status}});
    }
}

export const deletePost = postID => async dispatch => {
    try {
        await axios.delete(`https://arcane-tor-47520.herokuapp.com/api/posts/${postID}`);
        dispatch({type: DELETE_POST, payload: postID});
        dispatch(setAlert('Post Removed', 'Success'));
    } catch (err) {
        dispatch({type: POST_ERROR, payload: {msg: err.response.statusText, status: err.response.status}});
    }
}

export const addPost = formData => async dispatch => {
    try {
        const config = {
            headers: {
                'Content-Type': 'application/json'
            }
        }
        const res = await axios.post('https://arcane-tor-47520.herokuapp.com/api/posts', formData, config);
        dispatch({type: ADD_POST, payload: res.data});
        dispatch(setAlert('Post Created', 'Success'));
    } catch (err) {
        console.log(err);
        dispatch({type: POST_ERROR, payload: {msg: err.response.statusText, status: err.response.status}});
    }
}

export const addComment = (postID, formData) => async dispatch => {
    try {
        const config = {
            headers: {
                'Content-Type': 'application/json'
            }
        }
        const res = await axios.post(`https://arcane-tor-47520.herokuapp.com/api/posts/comment/${postID}`, formData, config);
        dispatch({type: ADD_COMMENT, payload: res.data});
        dispatch(setAlert('Comment Posted', 'Success'));
    } catch (err) {
        dispatch({type: POST_ERROR, payload: {msg: err.response.statusText, status: err.response.status}});
    }
}

export const deleteComment = (postID, commentID) => async dispatch => {
    try {
        await axios.delete(`https://arcane-tor-47520.herokuapp.com/api/posts/comment/${postID}/${commentID}`);
        dispatch({type: REMOVE_COMMENT, payload: commentID});
        dispatch(setAlert('Comment Posted', 'Success'));
    } catch (err) {
        dispatch({type: POST_ERROR, payload: {msg: err.response.statusText, status: err.response.status}});
    }
}

