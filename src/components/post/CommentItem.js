import React from 'react';
import {Link} from 'react-router-dom';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import Moment from 'react-moment';
import {deleteComment} from '../../actions/post';

const CommentItem = ({auth, postID, comment:{_id, text, name, avatar, user, date}, deleteComment}) => {
    return (
        <div className="post bg-white p-1 my-1">
            <div className="">
                <Link to={`/profile/${user}`}></Link>
                <img src={avatar} alt="avatar" className="round-img"/>
                <h4 className="text-primary">{name}</h4>
            </div>
            <div className="">
                <p className="my-1">{text}</p>
                <p className="post-date">Posted on <Moment format="YYYY/MM/DD">{date}</Moment></p>
                {!auth.loading && user === auth.user._id && 
                    <button type="button" className="btn btn-danger" onClick={e => deleteComment(postID, _id)}>
                        <i className="fas fa-times"></i>
                    </button>}
            </div>
        </div>
    )
}

CommentItem.propTypes = {
    postID: PropTypes.string.isRequired,
    comment: PropTypes.object.isRequired,
    auth: PropTypes.object.isRequired,
    deleteComment: PropTypes.func.isRequired
}

const mapStateToProps = state => ({
    auth: state.auth
})

export default connect(mapStateToProps, {deleteComment})(CommentItem)
