import React, {Fragment, useState} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {addExperience} from '../../actions/profile';
import {Link, withRouter} from 'react-router-dom';

const AddExperience = ({addExperience, history}) => {

    const [formData, setFormData] = useState({
        company: '',
        title: '',
        location: '',
        from: '',
        to: '',
        current: false,
        description: ''
    });
    const [toDateDisabled, toggleDisabled] = useState(false);

    const {
        company, 
        title,
        location,
        from,
        to,
        current,
        description
    } = formData;

    const onChange = e => {
        setFormData({
            ...formData,
            [e.target.name]: e.target.value
        });
    }

    const onCheckbox = e => {
        setFormData({
            ...formData,
            current: !current
        });
        toggleDisabled(!toDateDisabled);
    }

    const onSubmit = e => {
        e.preventDefault();
        addExperience(formData, history);
    }

    return (
        <Fragment>
            <h1 className="large text-primary">Add an experience</h1>
            <p className="lead">
                <i className="fas fa-code branch"></i>
                Add any developer/programming positions that you have had in the past
            </p>
            <small>* = required field</small>
            <form className="form" onSubmit={e => onSubmit(e)}>

                <div className="form-group">
                    <input type="text" name="title" placeholder="* Job Title" value={title} onChange={e => onChange(e)}/>
                </div>

                <div className="form-group">
                    <input type="text" name="company" placeholder="* Company" value={company} onChange={e => onChange(e)}/>
                </div>

                <div className="form-group">
                    <input type="text" name="location" placeholder="Location" value={location} onChange={e => onChange(e)}/>
                </div>

                <div className="form-group">
                    <h4>From Date</h4>
                    <input type="date" name="from" value={from} onChange={e => onChange(e)}/>
                </div>

                <div className="form-group">
                    <p><input type="checkbox" name="current" value={current} onChange={e => onCheckbox(e)} checked={current}/> Current Job</p>
                </div>

                <div className="form-group">
                    <h4>To Date</h4>
                    <input type="date" name="to" value={to} onChange={e => onChange(e)} disabled={toDateDisabled ? 'disabled' : ''}/>
                </div>

                <div className="form-group">
                    <textarea name="description" cols="30" rows="5" placeholder="Job Description" value={description} onChange={e => onChange(e)}></textarea>
                </div>

                <input type="submit" value="Add Experience" className="btn btn-primary my-1"/>
                <Link className="btn btn-light my-1" to="/dashboard.html">Go Back</Link>
            </form>
        </Fragment>
    )
}

AddExperience.propTypes = {
    addExperience: PropTypes.func.isRequired
}

export default connect(null, {addExperience})(withRouter(AddExperience))
